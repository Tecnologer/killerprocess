﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace KillerProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            KillProcess(args);

            if (NeedWait(args))
            {
                Console.WriteLine("Press enter to close...");
                Console.ReadLine();
            }
        }

        private static bool NeedWait(string[] args)
        {
            var waitRegex = new Regex("^(/|-)(w)|(wait)$", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            for (int i = args.Length - 1; i > 0; i--)
            {
                if (waitRegex.IsMatch(args[i].Trim()))
                {
                    return true;
                }
            }

            return false;
        }

        private static void KillProcess(string[] args)
        {
            try
            {
                if (args.Length < 1)
                {
                    Console.WriteLine("process not specified");
                    return;
                }

                if (!int.TryParse(args[0], out int pid))
                {
                    Console.WriteLine($"Closing process with name: {args[0]}");
                    KillProcess(args[0]);
                    return;
                }

                Console.WriteLine($"Closing process with ID: {pid}");
                KillProcess(pid);
            }
            catch
            {
                Console.WriteLine("ups!, something get wrong.");
                Console.WriteLine("Check your permision and try again");
            }
        }

        private static void KillProcess(int pid)
        {
            Process[] process = Process.GetProcesses();
            string pname = null;

            foreach (Process prs in process)
            {
                if (prs.Id != pid) continue;

                pname = prs.ProcessName;
                prs.Kill();
                break;
            }

            if(pname != null)
            {
                Console.WriteLine($"the process id={pid}, name={pname} is closed");
            }
            else
            {
                Console.WriteLine($"the process id={pid} is not found");
            }
        }

        private static void KillProcess(string name)
        {
            Process[] process = Process.GetProcesses();
            bool found = false;
            int pid = 0;
            foreach (Process prs in process)
            {
                if (!prs.ProcessName.Equals(name, StringComparison.OrdinalIgnoreCase)) continue;

                pid = prs.Id;
                prs.Kill();
                found = true;
                break;
            }

            if (found)
            {
                Console.WriteLine($"the process pid={pid},name={name} is closed");
            }
            else
            {
                Console.WriteLine($"the process with name {name} is not found");
            }
        }
    }
}
